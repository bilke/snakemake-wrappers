for f in snakemake.input:
    from jinja2 import Template

    with open(f) as in_file:
        template = Template(in_file.read())
    with open(f[:-3], "w") as out_file:
        out_file.write(template.render(config=snakemake.config))
