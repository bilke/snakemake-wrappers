Example usage:

```python
rule env_templates:
    input:
        "workflow/envs/{yaml}.j2"
    output:
        "workflow/envs/{yaml}"
    log:
        "results/env_template_{yaml}.log"
    wrapper:
        "master/helper/render-jinja"
```

Invoke `snakemake` with:

```bash
# Renders workflow/envs/environment.yaml.j to workflow/envs/environment.yaml:
snakemake --use-conda --wrapper-prefix ...  workflow/envs/environment.yaml
```
