# OGS Snakemake wrapper

This is a collection of reusable wrappers for OGS workflows using [Snakemake](https://snakemake.readthedocs.io).

## Usage notes

Add this following argument to every `snakemake`-call:

```bash
--wrapper-prefix https://gitlab.opengeosys.org/bilke/snakemake-wrappers/-/raw/
```

### Local git repo

Add this following argument to every `snakemake`-call:

```bash
--wrapper-prefix git+file://home/bilke/code/snakemake-wrappers # <- location of git repo
```
